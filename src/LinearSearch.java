import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-08-18.
 */
public class LinearSearch<T extends Comparable<T>> extends Search<T>{

    @Override
    public int doSearch(List<T> list, T searchElement) {
        int result = -1;


        for (int index = 0; index < list.size(); index++){
            if (list.get(index).compareTo(searchElement) == 0){
                System.out.println("Znalazlem samochod na indexie: " + index);
                result = index;
                break;
            }
        }
        return result;
    }

}
