/**
 * Created by RENT on 2017-08-18.
 */
public class Main {

    public static void main(String[] args) {

        Car audi4 = new Car("audi", "a4");
        Car audi6 = new Car("audi", "a6");

        audi4.compareTo(audi6);


        //od Tomka(przyklad, zeby rozjasnic niego obiektowosc i ~typy generyczne)
        BinarySearch binarySearch = new BinarySearch;
        LinearSearch linarSearch = new LinearSearch;

        Search<Feed> search = new LinearSearch;
         search.doSearch();

    }
}
/*
w klasie Search znajduje sie metoda "wypelnianiIntami"

 */
//komentarz by zobaczyc czy commit i push dziala na komputerze w domu

/*
kod ze slacka od Tomka:

public abstract class Search<T extends Comparable<T>> {

    public abstract int doSearch(List<T> list, T searchElement);
}

public class BinarySearch<T extends Comparable<T>> extends Search<T>

public class LinearSearch<T extends Comparable<T>> extends Search<T>

// Wywołanie powinno wyglądać mniej więcej w ten sposób (Integer - przykładowo)
// getSearchEngine wybiera algorytm do szukania losowo
Search<Integer> searchEngine = new SearchEngine<Integer>().getSearchEngine();
searchEngine.doSearch(...);
 */
