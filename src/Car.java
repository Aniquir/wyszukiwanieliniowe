/**
 * Created by RENT on 2017-08-18.
 */
public class Car implements Comparable<Car>{

    private String marka;
    private String model;

    public Car(String marka, String model) {
        this.marka = marka;
        this.model = model;
    }

    public String getMarka() {
        return marka;
    }

    public void setMarka(String marka) {
        this.marka = marka;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public int compareTo(Car o) {

        int result = -1;

        if (marka == o.marka && model == o.model){
                result = 1;
        }
        return result;
    }
}
